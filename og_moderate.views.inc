<?php
/**
 * hook_views_query_alter()
 */
function og_moderate_views_query_alter(&$view, &$query) {

  if ($view->name == 'og_members_admin' && $view->current_display == 'default') {
    $query->where[1]['conditions'][] = array(
            'field' => 'og_membership_users.state',
            'value' => array(OG_STATE_REQUEST_PENDING),
            'operator' => 'not in',
    );
  }
}

/* 
 *Implements hook_views_pre_render() 
 */
function og_moderate_views_pre_render(&$view) {
  if ($view->name == 'og_members_admin' && $view->current_display == 'default') {
//     display( $view->result );
//     die;
  }
}